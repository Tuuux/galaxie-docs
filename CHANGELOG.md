## Galaxie Docs
**0.5** - Jan 27 2023
* Remove green usage for testing
* Add YAML to Markdown support
* Convert python module in to pyproject.toml format
* Migrate to direnv

**0.4** - Sep 26 2021
* Fixe a trouble about argparse

**0.3** - Mar 26 2020
* GLXDocs is a safe thread singleton
* Better README

**0.2** - Mar 26 2020
* release

**0.2rc1** - Mar 26 2020
* add docstring
* configure sphinx
* optimize for readthedocs

**0.1** - Mar 25 2020
* first release

**0.1rc1** - Mar 25 2020
* glxdoc-md2html
