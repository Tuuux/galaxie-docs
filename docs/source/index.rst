.. title:: Galaxie Docs, survival md conversion tool
.. code-block::

                  ________        __                 __
                 /  _____/_____  |  | _____  ___  __|__| ____
                /   \  ___\__  \ |  | \__  \ \  \/  /  |/ __ \
                \    \_\  \/ __ \|  |__/ __ \_>    <|  \  ___/_
                 \________(______/____(______/__/\__\__|\_____/

==========================
Galaxie Docs Documentation
==========================

The Project
-----------
**Galaxie Docs** is a free software Tool Kit for convert document.

The lib is written with **Python** and deal with:
* Markdown to Html
* Yaml to Markdown

It is usable from CLI and Python code.

Installation
------------
* **Pre Version**: ```pip install galaxie-docs```
* **Dev Version**: ```pip install -i https://test.pypi.org/simple/ galaxie-docs```

Documentation
-------------
* **Readthedocs link**: `http://galaxie-docs.readthedocs.io <http://galaxie-docs.readthedocs.io>`_
* **Packages documentation**: `https://galaxie-docs.readthedocs.io/en/latest/glxdocs.html <https://galaxie-docs.readthedocs.io/en/latest/glxdocs.html>`_

The Mission
-----------
Galaxie use Markdown format as text format, then the lib have targeted to convert documents around MarkDown.

Contribute
----------
* **Issue Tracker**: `https://gitlab.com/Tuuux/galaxie-docs/issues <https://gitlab.com/Tuuux/galaxie-docs/issues>`_
* **Source Code**: `https://gitlab.com/Tuuux/galaxie-docs <https://gitlab.com/Tuuux/galaxie-docs>`_

Screenshots
-----------
**Pretty good**

.. figure::  https://gitlab.com/Tuuux/galaxie-docs/-/raw/9005032bfb4f6418465721ab25726bdcdf95a96e/docs/source/_images/result_01.png
   :align:   center

**Syntax coloration with css**

.. figure::  https://gitlab.com/Tuuux/galaxie-docs/-/raw/9005032bfb4f6418465721ab25726bdcdf95a96e/docs/source/_images/result_02.png
   :align:   center

Content
-------
.. toctree::
   :maxdepth: 2

   glxdocs
   modules

License
-------
GNU GENERAL PUBLIC LICENSE_ Version 3

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-curses/blob/master/LICENSE

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`
