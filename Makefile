.PHONY: help install-python header venv prepare docs tests clean
SHELL=/bin/sh

##
## ————————————————————————————————— GALAXIE-FORGE ————————————————————————————————
##
separator = "********************************************************************************"

.DEFAULT_GOAL = help

.PHONY: help
help: ## Display help
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/Makefile://' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-22s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: header
header:
	@ echo "***************************** GALAXIE DOCS MAKEFILE ****************************"
	@ echo "HOSTNAME	`uname -n`"
	@ echo "KERNEL RELEASE `uname -r`"
	@ echo "KERNEL VERSION `uname -v`"
	@ echo "PROCESSOR	`uname -m`"


.PHONY: install-python
install-python: header
	@ echo ""
	@ echo "**************************** INSTALL PYTHON PACKAGES ***************************"
	@ apt-get update && apt-get install -y python3 python3-pip python3-venv

##
## —————————————————————————— ENVIRONMENTS CONFIGURATION ——————————————————————————
##


.PHONY: prepare
prepare: ## Prepare requirements installation
	@echo "************************** PYTHON REQUIREMENTS *********************************"
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL PIP3" || \
	echo "[FAILED] INSTALL PIP3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL WHEEL" || \
	echo "[FAILED] INSTALL WHEEL"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL SETUPTOOLS" || \
	echo "[FAILED] INSTALL SETUPTOOLS"

	@pip3 install -U coverage --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL COVERAGE" || \
	echo "FAILED] INSTALL COVERAGE"

	@pip3 install -U -e . --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL GLXDOCS AS DEV MODE" || \
	echo "[FAILED] INSTALL GLXDOCS AS DEV MODE"

##
## —————————————————————————— TESTS ——————————————————————————
##

.PHONY: tests
tests: header ## Execute unittests
	@ echo ""
	@ echo "********************************** START TESTS *********************************"
	@ coverage run -m unittest -b tests/test_*.py tests/libs/test_*.py
	@ coverage report -m


##
## —————————————————————————— CLEAN ——————————————————————————
##

.PHONY: clean
clean: header ## Clean the workspace
	@ echo ""
	@ echo "*********************************** CLEAN UP ***********************************"
	rm -rf ./venv
	rm -rf ./direnv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf docs/build

##
## —————————————————————————— DOCUMENTATIONS ——————————————————————————
##
.PHONY: docs
docs: prepare ## Build documentation by using sphinx-apidoc and the Makefile inside ./docs directory
	@ echo ""
	@ echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@ pip3 install -r docs/requirements.txt --no-cache-dir --quiet
	@ cd docs &&\
	sphinx-apidoc -e -f -o source/ ../glxdocs/ &&\
	make html
