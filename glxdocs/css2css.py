import sys
import os
import re
from argparse import ArgumentParser, ONE_OR_MORE
from glxdocs.libs import ColorPalette


css2css_parser = ArgumentParser(
    prog="glx-css2css",
    add_help=True,
    epilog="Developed under GPLv3+ license",
    description="Galaxie Docs - Css to Css color palette conversion",
)
css2css_parser.add_argument(
    "--reset-palette",
    dest="reset_palette",
    action="store_true",
    default=False,
    help="reset the palette to zero color, you can add colors with multiple --add ADD calls.",
)

css2css_parser.add_argument(
    "--add",
    dest="add",
    action="append",
    default=[],
    help="add color by hex16 code '#FF0000', name: 'AURORA', 'FROST', 'POLAR_NIGHT', 'SNOW_STORM' "
         "or an existing file path it contain a color palette, one hex base 16 peer line ex: #FFFFFF . "
         "note: --add ADD can be call more of one time, no trouble to mixe them.",
)

css2css_parser.add_argument(
    "-i",
    "--interactive",
    dest="interactive",
    action="store_true",
    default=False,
    help="write a prompt for confirmation about: start processing, overwrite a existing file, by default no "
         "questions is asking. note: during prompt if response is 'N', a filename will be found automatically.",
)

css2css_parser.add_argument(
    "-y",
    "--yes",
    dest="yes",
    action="store_true",
    default=False,
    help="automatically by pass question by confirm with 'Y', that mean yes to continue and yes to "
         "overwrite existing files, note: by default prompt questions.",
)

css2css_parser.add_argument(
    "source",
    nargs=ONE_OR_MORE,
    default=None,
    metavar="SOURCE",
    help="a pathname of an existing file or directory, note: you can chain source "
         "like SOURCE [SOURCE ...] in that case TARGET will be consider as directory.",
)


class Css2Css:
    def __init__(self, **kwargs):
        self.reset_palette = kwargs.get("reset_palette", None)
        self.add = kwargs.get("add", None)

        self.interactive = kwargs.get("interactive", None)
        self.yes = kwargs.get("yes", None)

        self.__source = None
        self.source = kwargs.get("source", None)

        self.color_palette = ColorPalette()

        self.regex = r"#[0-9A-Fa-f]{3}(?:[0-9A-Fa-f]{3})?(?!$)"
        self.regex_mamed_color = r"(#(?:[0-9a-fA-F]{2}){2,4}$|(#[0-9a-fA-F]{3}$)|(rgb|hsl)a?\((-?\d+%?[,\s]+){2,3}\s*[\d\.]+%?\)$|black$|silver$|gray$|whitesmoke$|maroon$|red$|purple$|fuchsia$|green$|lime$|olivedrab$|yellow$|navy$|blue$|teal$|aquamarine$|orange$|aliceblue$|antiquewhite$|aqua$|azure$|beige$|bisque$|blanchedalmond$|blueviolet$|brown$|burlywood$|cadetblue$|chartreuse$|chocolate$|coral$|cornflowerblue$|cornsilk$|crimson$|darkblue$|darkcyan$|darkgoldenrod$|darkgray$|darkgreen$|darkgrey$|darkkhaki$|darkmagenta$|darkolivegreen$|darkorange$|darkorchid$|darkred$|darksalmon$|darkseagreen$|darkslateblue$|darkslategray$|darkslategrey$|darkturquoise$|darkviolet$|deeppink$|deepskyblue$|dimgray$|dimgrey$|dodgerblue$|firebrick$|floralwhite$|forestgreen$|gainsboro$|ghostwhite$|goldenrod$|gold$|greenyellow$|grey$|honeydew$|hotpink$|indianred$|indigo$|ivory$|khaki$|lavenderblush$|lavender$|lawngreen$|lemonchiffon$|lightblue$|lightcoral$|lightcyan$|lightgoldenrodyellow$|lightgray$|lightgreen$|lightgrey$|lightpink$|lightsalmon$|lightseagreen$|lightskyblue$|lightslategray$|lightslategrey$|lightsteelblue$|lightyellow$|limegreen$|linen$|mediumaquamarine$|mediumblue$|mediumorchid$|mediumpurple$|mediumseagreen$|mediumslateblue$|mediumspringgreen$|mediumturquoise$|mediumvioletred$|midnightblue$|mintcream$|mistyrose$|moccasin$|navajowhite$|oldlace$|olive$|orangered$|orchid$|palegoldenrod$|palegreen$|paleturquoise$|palevioletred$|papayawhip$|peachpuff$|peru$|pink$|plum$|powderblue$|rosybrown$|royalblue$|saddlebrown$|salmon$|sandybrown$|seagreen$|seashell$|sienna$|skyblue$|slateblue$|slategray$|slategrey$|snow$|springgreen$|steelblue$|tan$|thistle$|tomato$|transparent$|turquoise$|violet$|wheat$|white$|yellowgreen$|rebeccapurple$)"

    @property
    def source(self):
        return self.__source

    @source.setter
    def source(self, value):
        if isinstance(value, list):
            source_list = []
            for src in value:
                # Reset src_to_use value each iteration
                if isinstance(src, str):
                    src_to_use = src
                else:
                    src_to_use = None

                # Don't need to continue if path do not exist or cant be read
                if (
                    not src_to_use
                    or not os.access(src_to_use, os.F_OK, follow_symlinks=True)
                    or not os.access(src_to_use, os.R_OK, follow_symlinks=True)
                ):
                    continue

                # Links and dead links is supported
                src_to_use = self.lookup_file(src_to_use)

                # That is a file
                if os.path.isfile(src_to_use):
                    if (
                        self.lookup_file_supported_input_format(src_to_use)
                        and src_to_use not in source_list
                    ):
                        source_list.append(src_to_use)

                # That is a directory
                elif os.path.isdir(src_to_use):

                    # Check recursively for supported input file
                    for root, _, files in os.walk(src_to_use):
                        for file in files:
                            if (
                                self.lookup_file_supported_input_format(file)
                                and os.path.join(root, file) not in source_list
                            ):
                                source_list.append(os.path.join(root, file))

            if self.source != source_list:
                self.__source = source_list
        else:
            self.__source = []

    @staticmethod
    def lookup_file(path):
        if os.path.islink(path):
            while os.path.islink(path):
                path = os.readlink(path)
        if os.path.exists(path):
            return os.path.realpath(path)
        return None

    def lookup_file_supported_input_format(self, path):
        _, __, f_ext = self.lookup_file_into(path)
        # Match each file PIL input image format by extension name
        if f_ext.lower()[1:] in [
            "css",
        ]:
            return True
        return False

    @staticmethod
    def lookup_file_into(path):
        basedir = os.path.dirname(path)
        f_name, f_ext = os.path.splitext(os.path.basename(path).split("/")[-1])
        return basedir, f_name, f_ext

    def lookup_file_get_next_non_existing_filename(self, path):
        basedir, f_name, f_ext = self.lookup_file_into(path)
        if self.interactive is False and self.yes is False:
            if os.path.exists(path):
                count = 1
                while os.path.exists(f"{os.path.join(basedir, f_name)}-{count}{f_ext}"):
                    count += 1  # pragma: no cover
                return f"{os.path.join(basedir, f_name)}-{count}{f_ext}"
            return path
        elif self.interactive is True and self.yes is False:  # pragma: no cover
            if os.path.exists(path):
                if self.ask_to_continue(f"File all ready exist: {path}? (Y/n) "):
                    return path
                else:
                    count = 1
                    while os.path.exists(
                        f"{os.path.join(basedir, f_name)}-{count}{f_ext}"
                    ):
                        count += 1
                    return f"{os.path.join(basedir, f_name)}-{count}{f_ext}"
            return path
        # elif self.interactive is True and self.yes is True:
        #     return path
        # elif self.interactive is False and self.yes is True:
        #     return path
        return path

    @staticmethod
    def ask_to_continue(text: str, startswith: str = "N"):
        if not input(f"{text}").upper().startswith(startswith):
            return True
        return False

    def pre_processing_palette(self):
        if self.reset_palette:
            self.color_palette.reset()
        for color in self.add:

            # You can add color also by their hex code
            if f"{color}".startswith("#") and len(f"{color}") == 7:
                self.color_palette.add(color)

            # Here we can consider the name is a path of a filename to load
            if os.access(color, os.F_OK, follow_symlinks=True):
                with open(color) as file_to_import:
                    for line in file_to_import:
                        if f"{line}".startswith("#") and len(f"{line}".strip()) == 7:
                            self.color_palette.add(f"{line}".strip())

            # You can add color also by their name
            # if f"{color}" in ["AURORA", "FROST", "POLAR_NIGHT", "SNOW_STORM"]:
            #     self.go_nord.add_file_to_palette(getattr(NordPaletteFile, f"{color}"))

        # The end of the world
        if len(self.color_palette.get()) == 0:
            sys.stdout.write("No color is load at all\n")
            return 1

        return 0

    def processing(self):
        if self.source:
            try:
                sys.stdout.write(f"{''.center(os.get_terminal_size().columns, '=')}\n")  # pragma: no cover
            except OSError:
                sys.stdout.write(f"{''.center(80, '=')}\n")
            sys.stdout.write("Processing\n")
            sys.stdout.flush()

            for src_path in self.source:

                # sys.stdout.write(f"Convert {src_path}\n")
                # sys.stdout.flush()


                with open(src_path, "r") as src_fd:
                    src_fd_lines = src_fd.readlines()

                line_number = 1
                color_palette_to_replace = ColorPalette()
                sys.stdout.write(f"Searching into: {os.path.relpath(src_path)}\n")
                for line in src_fd_lines:
                    matches = re.findall(self.regex, line, re.MULTILINE)

                    # A Color are found
                    if matches:
                        for match in matches:
                            color_palette_to_replace.add(match)
                            sys.stdout.write(f"{line_number}: {match} -> {self.color_palette.closest(match)}\n")


                    line_number += 1


                # If colors to convert found
                if len(color_palette_to_replace.get()):
                    
                    # Summary of what have been found
                    sys.stdout.write("Found colors: \n")
                    for color in color_palette_to_replace.get():
                        sys.stdout.write(f"\t{color}\n")
    
                    # About convertion
                    sys.stdout.write("Convertion colors: \n")
                    for color in color_palette_to_replace.get():
                        sys.stdout.write(f"\t{color} -> {self.color_palette.closest(color)}\n")
    
                    # Convertion
                    converted_lines = []
                    tested_line = ""
                    for line in src_fd_lines:
                        tested_line = line
    
                        for color in color_palette_to_replace.get():
                            tested_line = tested_line.replace(f"{color.upper()}", f"{self.color_palette.closest(color)}")
                            tested_line = tested_line.replace(f"{color.lower()}", f"{self.color_palette.closest(color)}")
    
                        converted_lines.append(f"{tested_line}")
    
    
                    # Final export
                    if self.yes:
                        with open(src_path, "r") as dst_fd:
                            for line in converted_lines:
                                dst_fd.write(line)
                    elif self.yes is False and self.interactive:
                        if self.yes is False:
                            sys.stdout.flush()
                            # os.path.relpath(src_path)
                            if not self.ask_to_continue(f"Overwirte ? {os.path.relpath(src_path)} (Y/n) "):
                                continue
                            with open(src_path, "r") as dst_fd:
                                for line in converted_lines:
                                    dst_fd.write(line)
                    else:
                        sys.stdout.write("Nothing to convert\n")
                    
        else:
            sys.stdout.write("Nothing to process\n")
        sys.stdout.flush()

    def post_processing(self):
        pass

    def print_summary(self):
        # header
        sys.stdout.write("\n")
        try:
            sys.stdout.write(
                f"{' CSS2CSS '.center(os.get_terminal_size().columns, '=')}\n"
            )   # pragma: no cover
        except OSError:
            sys.stdout.write(
                f"{' CSS2CSS '.center(80, '=')}\n"
            )

        # setting
        sys.stdout.write(
            f"Reset Palette: {self.reset_palette}\n"
            f"Interactive: {self.interactive}\n"
            f"Yes: {self.yes}\n"
        )

        # Palette
        # colors from add
        sys.stdout.write("Add color: ")
        if self.add:
            sys.stdout.write("\n")
            for add in self.add:
                sys.stdout.write(f"\t{add}\n")
        else:
            sys.stdout.write("None\n")

        # colors from color_palette point of view
        sys.stdout.write(f"Color{'s' if len(self.source)>1 else ''}: ")
        if self.color_palette.get():
            sys.stdout.write("\n")
            for hex16 in self.color_palette.get():
                # sys.stdout.write(
                #     f"\tHex: #{hex16}, RGB: ({rgb[0]},{rgb[1]},{rgb[2]})\n"
                # )
                sys.stdout.write(
                    f"\tHex: {hex16}\n"
                )
        else:
            sys.stdout.write("None\n")

        # source
        sys.stdout.write(f"Source{'s' if len(self.source) > 1 else ''}:")
        if len(self.source) > 1:
            sys.stdout.write("\n")
            for source in self.source:
                sys.stdout.write(f"\t{source}\n")
        elif len(self.source) == 1:
            sys.stdout.write(f" {self.source[0]}\n")
        else:
            sys.stdout.write(f" None\n")

        # footer
        try:
            sys.stdout.write(f"{''.center(os.get_terminal_size().columns, '=')}\n")  # pragma: no cover
        except OSError:
            sys.stdout.write(f"{''.center(80, '=')}\n")

        sys.stdout.flush()


    def run(self):
        if self.pre_processing_palette():
            sys.stdout.write("Trouble during color palette initialize\n")
            sys.stdout.flush()
            return 1

        self.print_summary()

        try:
            if self.yes is False:
                sys.stdout.flush()
                if not self.ask_to_continue("Do you want to continue ? (Y/n) "):
                    sys.stdout.write("All operations are stop by the user\n")
                    sys.stdout.flush()
                    return 0


            self.processing()
            self.post_processing()
        except KeyboardInterrupt:
            sys.stdout.write("\nAll operations are interrupted by the user\n")
            sys.stdout.flush()

        # If we are here that because all is finish, we can exit with
        return 0

def main():  # pragma: no cover
    """
    The entry point of ``glx-css2css``, it is use by setup-tools as console script.
    Tha function define the CLI parser and the Css2Css class object
    """
    args = css2css_parser.parse_args()
    css2css = Css2Css(
        reset_palette=args.reset_palette,
        add=args.add,
        interactive=args.interactive,
        yes=args.yes,
        source=args.source,
    )
    sys.exit(css2css.run())
