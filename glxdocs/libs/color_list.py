class ColorList(list):

    def __init__(self, iterable=None):
        """

        :rtype: iterable
        """
        if iterable:
            super().__init__(self.__validate(item) for item in iterable)
        else:
            super().__init__(self)

    def __setitem__(self, index, item):
        if item not in self:
            super().__setitem__(index, self.__validate(item))

    def insert(self, index, item):
        if item not in self:
            super().insert(index, self.__validate(item))

    def append(self, item):
        if item not in self:
            super().append(self.__validate(item))

    def extend(self, other):
        if isinstance(other, type(self)):
            tmp_list = []
            for item in other:
                if item not in self:
                    tmp_list.append(item)
            super().extend(tmp_list)
        else:
            tmp_list = []
            for item in other:
                if item not in self:
                    tmp_list.append(item)
            super().extend(self.__validate(item) for item in tmp_list)

    @staticmethod
    def __validate(value):
        if isinstance(value, int):
            return value
        raise TypeError(f"int instance expected, got {type(value).__name__}")
