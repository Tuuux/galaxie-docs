import webcolors

class Color:
    """
    Represents an color.

    Color store any imported color as a int
    """

    def __init__(self, value: int or str = None):
        self.base = 0
        if isinstance(value, int):
            self.base = value
            
        elif isinstance(value, str) and str(value).startswith("#"):
            try:
                self.base = int(webcolors.normalize_hex(value).lstrip("#"), 16)
            except ValueError:
                pass
        elif isinstance(value, str):
            try:
                self.base = int(webcolors.name_to_hex(value).lstrip("#"), 16)
            except ValueError:
                pass
            
        elif isinstance(value, tuple):
            if len(value) == 3:
                if isinstance(value[0], int) and isinstance(value[1], int) and isinstance(value[2], int):
                    try:
                        self.base = int(webcolors.rgb_to_hex(value).lstrip("#"), 16)
                    except ValueError:
                        pass
                elif isinstance(value[0], str) and isinstance(value[1], str) and isinstance(value[2], str):
                    try:
                        self.base = int(webcolors.rgb_percent_to_hex(value).lstrip("#"), 16)
                    except ValueError:
                        pass
    @classmethod
    def from_int(cls, color: int):
        """
        Creates a Color class from the specified integer.

        .. seealso::
         - :py:func:`~glxdocs.lib.Color.to_int()`

        :param color: HTML color HEX
        :type color: str
        :return: The :py:class:`~glxdocs.lib.Color` that this method creates.
        :rtype: :py:class:`~glxdocs.lib.Color`
        """
        return cls(color)

    @classmethod
    def from_hex(cls, color: str):
        """
        Creates a Color class from the specified HTML HEX palette.

        .. seealso::
         - :py:func:`~glxdocs.lib.Color.to_hex()`

        :param color: HTML color HEX
        :type color: str
        :return: The :py:class:`~glxdocs.lib.Color` that this method creates.
        :rtype: :py:class:`~glxdocs.lib.Color`
        """
        return cls(color)

    def to_int(self):
        """
        Gets the 16-bit value of this Color class.

        :return: The 32-bit ARGB value :py:class:`~microGUI.Type.Color.ColorType` of \
                 this :py:class:`~microGUI.Drawing.Color.Color`.
        :rtype: int
        """
        return self.base

    def to_hex(self):
        """
        Creates a Color class from the specified HTML HEX palette.
        The alpha value is implicitly 255 (fully opaque).

        .. seealso::
         - :py:func:`~glxdocs.lib.Color.from_hex()`

        :return: The color base as HTML color HEX
        :rtype: str
        """
        return f"#{self.base:06X}"
