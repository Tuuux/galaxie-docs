from glxdocs.libs import Color
from glxdocs.libs import ColorList


class ColorPalette:
    def __init__(self):
        self.__colors = None
        self.colors = None

    @property
    def colors(self):
        """
        Strore the ColorList object

        :return: The stored value
        :rtype: list
        """
        return self.__colors

    @colors.setter
    def colors(self, value: ColorList = None):
        if value is None:
            value = ColorList()

        if not isinstance(value, ColorList):
            raise TypeError(
                "'colors' property value must be a ColorList instance or None"
            )

        if self.colors != value:
            self.__colors = value

    def get(self):
        return [Color(color).to_hex() for color in iter(self.colors)]

    def add(self, value):
        self.colors.append(Color(value).to_int())

    def delete(self, value):
        self.colors.remove(value)

    def clear(self):
        self.colors.clear()

    def reset(self):
        self.colors = None

    def closest(self, color):
        aux = []
        for valor in iter(self.colors):
            aux.append(abs(Color(color).to_int() - valor))
        index = aux.index(min(aux))

        return Color(list(self.colors)[index]).to_hex()
