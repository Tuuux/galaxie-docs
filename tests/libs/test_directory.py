from os import getcwd
from unittest import TestCase, main

from glxdocs.libs.directory import Directory


class TestFile(TestCase):
    def setUp(self) -> None:
        self.directory = Directory()

    def test_path(self):
        self.directory.path = "../"
        self.assertEqual("../", self.directory.path)

        self.directory.path = None
        self.assertEqual(getcwd(), self.directory.path)

        self.assertRaises(TypeError, setattr, self.directory, "path", 42)


if __name__ == "__main__":
    main()
