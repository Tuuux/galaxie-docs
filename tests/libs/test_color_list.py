import unittest

from glxdocs.libs import ColorList


class TestColorList(unittest.TestCase):
    def setUp(self):
        self.color_list = ColorList()

    def test_set_item(self):
        self.color_list = ColorList([4, 5, 6])
        self.color_list[0] = 1
        self.color_list[1] = 2
        self.color_list[2] = 3
        self.assertEqual(self.color_list, [ 1 , 2, 3])

    def test_insert(self):
        self.color_list = ColorList([ 1 , 2, 3])

        self.color_list.insert(0, 0)
        self.assertEqual(self.color_list, [0, 1, 2, 3])

        self.color_list.insert(0, 0)
        self.assertEqual(self.color_list, [0, 1, 2, 3])

    def test_append(self):
        self.color_list = ColorList([1, 2, 3])

        self.color_list.append(0)
        self.assertEqual(self.color_list, [1, 2, 3, 0])

        self.color_list.append(0)
        self.assertEqual(self.color_list, [1, 2, 3, 0])

    def test_extend(self):
        self.color_list = ColorList([1, 3, 4])
        self.color_list.extend([2, 4])
        self.assertEqual(self.color_list, [1, 3, 4, 2])

        self.color_list = ColorList([1, 2, 3])
        self.color_list.extend([4, 5, 6])
        self.assertEqual(self.color_list, [1, 2, 3, 4, 5, 6])

        color_list1 = ColorList([1,2,3])
        color_list2 = ColorList([3,4,5,6])

        color_list1.extend(color_list2)
        self.assertEqual(color_list1, [1, 2, 3, 4, 5, 6])

    def test_raises(self):
        self.assertRaises(TypeError, self.color_list.extend, None)
        self.assertRaises(TypeError, self.color_list.extend, [ColorList(), None])

if __name__ == '__main__':
    unittest.main()
