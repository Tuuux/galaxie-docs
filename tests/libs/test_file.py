import unittest
import os
import io
from io import StringIO
from unittest.mock import patch
import random
from glxdocs.libs.file import File
from glxdocs.libs.directory import Directory


def new_id():
    """
    Generate a GLXCurses ID like 'E59E8457', two chars by two chars it's a random HEX

    **Default size:** 8
    **Default chars:** 'ABCDEF0123456789'

    **Benchmark**
       +----------------+---------------+----------------------------------------------+
       | **Iteration**  | **Duration**  | **CPU Information**                          |
       +----------------+---------------+----------------------------------------------+
       | 10000000       | 99.114s       | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 1000000        | 9.920s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 100000         | 0.998s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 10000          | 0.108s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+

    :return: a string it represent a unique ID
    :rtype: str
    """
    return "%02x%02x%02x%02x".upper() % (
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
    )


def get_os_temporary_dir():
    """
    Get the OS default dir , the better as it can.

    It suppose to be cross platform

    :return: A tmp dir path
    :rtype: str
    """
    text_flags = os.O_RDWR | os.O_CREAT | os.O_EXCL
    if hasattr(os, "O_NOFOLLOW"):
        text_flags |= os.O_NOFOLLOW

    bin_flags = text_flags
    if hasattr(os, "O_BINARY"):  # pragma: no cover
        bin_flags |= os.O_BINARY

    directory_list = list()

    # First, try the environment.
    for envname in "TMPDIR", "TEMP", "TMP":
        dirname = os.getenv(envname)
        if dirname:
            directory_list.append(os.path.abspath(dirname))

    directory_list.extend(["/tmp", "/var/tmp", "/usr/tmp"])

    for directory in directory_list:
        if directory != os.path.curdir:
            directory = os.path.abspath(directory)

        name = str("wg-config-")
        name += str(new_id())
        name += str(".cp")
        filename = os.path.join(directory, name)
        try:
            fd = os.open(filename, bin_flags, 0o600)
            try:
                try:
                    with io.open(fd, "wb", closefd=False) as fp:
                        fp.write(b"Test")
                finally:
                    os.close(fd)
            finally:
                os.unlink(filename)
            return directory

        except PermissionError:  # pragma: no cover
            break  # no point trying more names in this directory
        except OSError:  # pragma: no cover
            break  # no point trying more names in this directory
    raise FileNotFoundError("No usable temporary directory found in %s" % directory_list)


class TestFile(unittest.TestCase):
    def setUp(self) -> None:
        self.file = File()

    def test_extension(self):
        self.file.extension = "cpuinfo"

        self.assertEqual(self.file.extension, "cpuinfo")

        self.file.extension = None
        self.assertIsNone(self.file.extension)

        self.assertRaises(TypeError, setattr, self.file, "extension", 42)

    def test_name(self):
        self.file.name = "cpuinfo"

        self.assertEqual(self.file.name, "cpuinfo")

        self.file.name = None
        self.assertIsNone(self.file.name)

        self.assertRaises(TypeError, setattr, self.file, "name", 42)

    def test_directory(self):
        object_a = Directory()
        object_b = Directory()

        self.assertNotEqual(object_a, object_b)

        self.file.directory = object_a

        self.assertEqual(self.file.directory, object_a)
        self.assertNotEqual(self.file.directory, object_b)

        self.file.directory = None
        self.assertNotEqual(self.file.directory, object_a)
        self.assertNotEqual(self.file.directory, object_b)
        self.assertTrue(isinstance(self.file.directory, Directory))

        self.assertRaises(TypeError, setattr, self.file, "directory", 42)

    def test_path(self):
        file = File()
        file.path = "/proc/cpuinfo"

        self.assertEqual(file.path, "/proc/cpuinfo")
        self.assertEqual(file.directory.path, "/proc")
        self.assertEqual(file.name, "cpuinfo")
        self.assertIsNone(file.extension)

        file.path = "/proc/cpuinfo.txt"

        self.assertEqual("/proc/cpuinfo.txt", file.path)
        self.assertEqual("/proc", file.directory.path)
        self.assertEqual("cpuinfo", file.name)
        self.assertEqual(".txt", file.extension)

        file.path = "/proc/cpuinfo.txt.gz"

        self.assertEqual(file.path, "/proc/cpuinfo.txt.gz")
        self.assertEqual(file.directory.path, "/proc")
        self.assertEqual(file.name, "cpuinfo")
        self.assertEqual(file.extension, ".txt.gz")

        file.path = "/proc/.bashrc"

        self.assertEqual(file.path, "/proc/.bashrc")
        self.assertEqual(file.directory.path, "/proc")
        self.assertEqual(file.name, ".bashrc")
        self.assertIsNone(file.extension)

        file.path = "-"
        self.assertIsNone(file.path)
        self.assertEqual(file.directory.path, os.getcwd())
        self.assertEqual(file.name, "-")
        self.assertIsNone(file.extension)

        file.path = "/proc/.bash.rc"
        self.assertEqual(file.path, "/proc/.bash.rc")
        self.assertEqual(file.directory.path, "/proc")
        self.assertEqual(file.name, ".bash")
        self.assertEqual(file.extension, ".rc")

        file.path = None
        self.assertIsNone(file.extension)
        self.assertIsNone(file.name)
        self.assertEqual(file.directory.path, os.getcwd())
        self.assertIsNone(file.path)

        self.assertRaises(TypeError, setattr, file, "path", 42)

    def test_overwrite(self):
        file = File()
        self.assertFalse(file.overwrite)
        file.overwrite = True
        self.assertTrue(file.overwrite)
        file.overwrite = False
        self.assertFalse(file.overwrite)
        file.overwrite = None
        self.assertFalse(file.overwrite)

        self.assertRaises(TypeError, setattr, file, "overwrite", 42)

    def test_is_binary(self):
        file = File()
        file.path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "glxdocs.png"))
        self.assertTrue(file.is_binary())
        file.path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "glxdocs.txt"))
        self.assertFalse(file.is_binary())
        file.path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../data/glxwireguard-config.i_do_not_exist"
        )
        self.assertRaises(FileNotFoundError, file.is_binary)

    def test_is_text(self):
        file = File()
        file.path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "glxdocs.png"))
        self.assertFalse(file.is_text())
        file.path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "glxdocs.txt"))
        self.assertTrue(file.is_text())
        file.path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../data/glxdocs.i_do_not_exist"
        )
        self.assertRaises(FileNotFoundError, file.is_text)

    def test_found_best_output_file_name(self):
        # Clean everything
        file0_path = os.path.join(get_os_temporary_dir(), "glxwireguard-config-tests-UtilsFile{0}".format(""))

        file1_path = os.path.join(
            get_os_temporary_dir(),
            "glxwireguard-config-tests-UtilsFile{0}".format("-1"),
        )
        file2_path = os.path.join(
            get_os_temporary_dir(),
            "glxwireguard-config-tests-UtilsFile{0}".format("-2"),
        )
        file3_path = os.path.join(
            get_os_temporary_dir(),
            "glxwireguard-config-tests-UtilsFile{0}".format("-3"),
        )

        if os.path.isfile(file0_path):
            os.remove(file0_path)
        if os.path.isfile(file1_path):
            os.remove(file1_path)
        if os.path.isfile(file2_path):
            os.remove(file2_path)
        if os.path.isfile(file3_path):
            os.remove(file3_path)

        self.assertFalse(os.path.isfile(file0_path))
        self.assertFalse(os.path.isfile(file1_path))
        self.assertFalse(os.path.isfile(file2_path))
        self.assertFalse(os.path.isfile(file3_path))

        # Create files
        file0 = os.open(file0_path, os.O_RDWR | os.O_CREAT)
        os.close(file0)
        file1 = os.open(file1_path, os.O_RDWR | os.O_CREAT)
        os.close(file1)
        file2 = os.open(file2_path, os.O_RDWR | os.O_CREAT)
        os.close(file2)
        self.assertTrue(os.path.isfile(file0_path))
        self.assertTrue(os.path.isfile(file1_path))
        self.assertTrue(os.path.isfile(file2_path))

        file = File()
        file.path = file0_path

        file.overwrite = False
        self.assertEqual(file.found_best_output_file_name(), file3_path)

        file.overwrite = True
        self.assertEqual(file.found_best_output_file_name(), file.path)

        # Clean everything
        if os.path.isfile(file0_path):
            os.remove(file0_path)
        if os.path.isfile(file1_path):
            os.remove(file1_path)
        if os.path.isfile(file2_path):
            os.remove(file2_path)
        if os.path.isfile(file3_path):
            os.remove(file3_path)

        self.assertFalse(os.path.isfile(file0_path))
        self.assertFalse(os.path.isfile(file1_path))
        self.assertFalse(os.path.isfile(file2_path))
        self.assertFalse(os.path.isfile(file3_path))

        # Test with extension
        file0_path = os.path.join(
            get_os_temporary_dir(),
            "glxcurses-tests-UtilsFile{0}.txt".format(""),
        )

        file1_path = os.path.join(
            get_os_temporary_dir(),
            "glxcurses-tests-UtilsFile{0}.txt".format("-1"),
        )
        file2_path = os.path.join(
            get_os_temporary_dir(),
            "glxcurses-tests-UtilsFile{0}.txt".format("-2"),
        )
        file3_path = os.path.join(
            get_os_temporary_dir(),
            "glxcurses-tests-UtilsFile{0}.txt".format("-3"),
        )

        if os.path.isfile(file0_path):
            os.remove(file0_path)
        if os.path.isfile(file1_path):
            os.remove(file1_path)
        if os.path.isfile(file2_path):
            os.remove(file2_path)
        if os.path.isfile(file3_path):
            os.remove(file3_path)

        self.assertFalse(os.path.isfile(file0_path))
        self.assertFalse(os.path.isfile(file1_path))
        self.assertFalse(os.path.isfile(file2_path))
        self.assertFalse(os.path.isfile(file3_path))

        # Create files
        file0 = os.open(file0_path, os.O_RDWR | os.O_CREAT)
        os.close(file0)
        file1 = os.open(file1_path, os.O_RDWR | os.O_CREAT)
        os.close(file1)
        file2 = os.open(file2_path, os.O_RDWR | os.O_CREAT)
        os.close(file2)
        self.assertTrue(os.path.isfile(file0_path))
        self.assertTrue(os.path.isfile(file1_path))
        self.assertTrue(os.path.isfile(file2_path))

        file = File()
        file.path = file0_path

        file.overwrite = False
        self.assertEqual(file.found_best_output_file_name(), file3_path)

        file.overwrite = True
        self.assertEqual(file.found_best_output_file_name(), file.path)

        # Clean everything
        if os.path.isfile(file0_path):
            os.remove(file0_path)
        if os.path.isfile(file1_path):
            os.remove(file1_path)
        if os.path.isfile(file2_path):
            os.remove(file2_path)
        if os.path.isfile(file3_path):
            os.remove(file3_path)

        self.assertFalse(os.path.isfile(file0_path))
        self.assertFalse(os.path.isfile(file1_path))
        self.assertFalse(os.path.isfile(file2_path))
        self.assertFalse(os.path.isfile(file3_path))

    def test_mode(self):
        file = File()
        file.mode = "wt"
        self.assertEqual("wt", file.mode)

        file.mode = None
        self.assertEqual("rt", file.mode)

        self.assertRaises(TypeError, setattr, file, "mode", 42)
        self.assertRaises(ValueError, setattr, file, "mode", "hello42")

    def test_smart_open(self):
        file = File()
        file.path = os.path.abspath(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "glxdocs.txt"))
        file.mode = 'r'

        with file.smart_open() as handle:
            self.assertEqual(["hello42"],  handle.readlines())

        # Test stdout
        file = File()
        file.path = "-"
        file.mode = "w"

        with patch("sys.stdout", new=StringIO()) as fake_out:
            with file.smart_open() as opened_file:
                opened_file.write("hello42")
            self.assertEqual("hello42", fake_out.getvalue())

        # Test stdin
        file = File()
        file.path = "-"
        file.mode = "r"

        with patch("sys.stdin", new=StringIO()) as fake_in:
            with file.smart_open() as opened_file:
                opened_file.write("hello42")
            self.assertEqual("hello42", fake_in.getvalue())




if __name__ == "__main__":
    unittest.main()
