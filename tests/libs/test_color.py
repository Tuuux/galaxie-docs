import unittest
from glxdocs.libs import Color

class TestColor(unittest.TestCase):
    def setUp(self):
        self.color = Color()

    def test_class(self):
        color = Color.from_hex('#FF0000')
        self.assertEqual(color.base, 16711680)

        color = Color.from_int(16711680)
        self.assertEqual(color.base, 16711680)

        self.assertEqual(Color.from_int(16711680).to_hex(), '#FF0000')
        self.assertEqual(Color.from_hex('#FF0000').to_int(), 16711680)

if __name__ == '__main__':
    unittest.main()
