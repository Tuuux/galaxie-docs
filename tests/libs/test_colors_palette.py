import unittest
from glxdocs.libs import ColorPalette
from glxdocs.libs import ColorList

class TestColorPalette(unittest.TestCase):
    def setUp(self):
        self.color_palette = ColorPalette()

    def test_color_list(self):

        color_list1 = ColorList([1,2,3])
        color_list2 = ColorList([4,5,6])

        self.color_palette.colors = color_list1
        self.assertEqual(self.color_palette.colors, color_list1)

        self.color_palette.colors = color_list2
        self.assertEqual(self.color_palette.colors, color_list2)

        self.color_palette.colors = None
        self.assertNotEqual(self.color_palette.colors, color_list1)
        self.assertNotEqual(self.color_palette.colors, color_list2)
        self.assertTrue(isinstance(self.color_palette.colors, ColorList))

        self.assertRaises(TypeError, setattr, self.color_palette,  "colors", 42)

    def test_get(self):
        self.color_palette.colors.append(42)
        self.assertEqual(self.color_palette.get(), ['#00002A'])

    def test_add(self):
        self.color_palette.add(42)
        self.assertEqual(self.color_palette.colors, [42])

    def test_delete(self):
        self.color_palette.colors.append(42)
        self.assertEqual(self.color_palette.colors, [42])
        self.color_palette.delete(42)
        self.assertEqual(self.color_palette.colors, [])

    def test_clear(self):
        self.color_palette.colors.append(42)
        self.assertEqual(self.color_palette.colors, [42])
        self.color_palette.clear()
        self.assertEqual(self.color_palette.colors, [])

    def test_reset(self):
        self.color_palette.colors.append(42)
        self.assertEqual(self.color_palette.colors, [42])
        self.color_palette.reset()
        self.assertEqual(self.color_palette.colors, [])

    def test_closet(self):
        nord_palette = [
            '#2E3440',
            '#3B4252',
            '#434C5E',
            '#4C566A',
            '#D8DEE9',
            '#E5E9F0',
            '#ECEFF4',
            '#8FBCBB',
            '#88C0D0',
            '#81A1C1',
            '#5E81AC',
            '#BF616A',
            '#D08770',
            '#EBCB8B',
            '#A3BE8C',
            '#B48EAD'
        ]

        for color in nord_palette:
            self.color_palette.add(color)

        self.assertEqual(nord_palette, self.color_palette.get())

        self.assertEqual(self.color_palette.closest('#B48EAD'), '#B48EAD')
        self.assertEqual(self.color_palette.closest('#FFFFFF'), '#ECEFF4')
        self.assertEqual(self.color_palette.closest('#000000'), '#2E3440')

if __name__ == '__main__':
    unittest.main()
